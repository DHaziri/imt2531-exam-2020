#version 330 core

//
// Shader for things that need light (3D)
//


// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform float ambientPower;
uniform sampler2D myTextureSampler;
uniform vec3 LightPosition_worldspace;	// Lights position on the level
uniform vec3 LightColor;					// The colour of the different lights
uniform bool LightOn;					// If the lights are in use at all



void main() 
{
	float LightPower = 20.5f;


	// Material properties
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);
	vec3 MaterialDiffuseColor;
	
	float height = Position_worldspace.y;

	vec3 highCol =  vec3(0.7, 0.5, 0.3);
	vec3 lowCol =  vec3(0.9, 0.9, 0.2);
	vec3 midCol =  vec3(0.3, 0.8, 0.1);

	float lowHeight = 0.2;
	float highHeight = 7.0;

	if (height <= lowHeight)
	{
		MaterialDiffuseColor = lowCol;
	}
	else if ( height >= highHeight)
	{
		
		MaterialDiffuseColor = highCol;
	}
	else
	{
		float blend = (height - lowHeight)/(highHeight-lowHeight);
		MaterialDiffuseColor = (1.0f-blend)*midCol + blend*highCol;
	}


	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;

	if(!LightOn)
	{
		vec3 MaterialAmbientColor = vec3(0.5,0.0,0.1) * MaterialDiffuseColor;
	}


	// The final light shown
	vec3 totalLight = MaterialAmbientColor * ambientPower;


	if(LightOn)
	{
		// Distance to the light
		float distance = length( LightPosition_worldspace - Position_worldspace );

		// Normal of the computed fragment, in camera space
		vec3 n = normalize( Normal_cameraspace );
		// Direction of the light (from the fragment to the light)
		vec3 l = normalize( LightDirection_cameraspace );
		// Cosine of the angle between the normal and the light direction, 
		// clamped above 0
		//  - light is at the vertical of the triangle -> 1
		//  - light is perpendicular to the triangle -> 0
		//  - light is behind the triangle -> 0
		float cosTheta = clamp( dot( n,l ), 0,1 );
	
		// Eye vector (towards the camera)
		vec3 E = normalize(EyeDirection_cameraspace);
		// Direction in which the triangle reflects the light
		vec3 R = reflect(-l,n);
		// Cosine of the angle between the Eye vector and the Reflect vector,
		// clamped to 0
		//  - Looking into the reflection -> 1
		//  - Looking elsewhere -> < 1
		float cosAlpha = clamp( dot( E,R ), 0,1 );

		totalLight += 
			// Diffuse : "color" of the object
			MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance) +
			// Specular : reflective highlight, like a mirror
			MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,10) / (distance*distance);
	}

	color = vec4(totalLight, 1);
}