#version 330 core

//
// Shader for things that dont need light (2D)
//

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

void main()
{
	// If alpha is too small, dont draw
	if(texture(myTextureSampler, UV).a < 0.01)
	{
		discard;
	}
	// Output color = color of the texture at the specified UV
	color = texture( myTextureSampler, UV ).rgba;
}