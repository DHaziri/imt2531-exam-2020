# imt2531_Exam


Donjetë Haziri
donjeteh


##How to start the file
This guide assumes that you have both cMake and Visual Studio.

- Open cMake.
- Write "path/imt2531-exam-2020" into "Where is the source code" and "path/imt2531-exam-2020/build" into "Where to build the binaries"
- "Configurate", and build build
- "Generate"
- "Open project"
- Left click "ALL_BUILD" and select "Build"
- Left click "Solution 'exam' ", and select "Properties"
- Change "Single startup project" from "ALL_BUILD" to "exam"



## Controls

T - Toggle between Camera angels/modes
 Starts at third person, press for global view, press again for first person, press again for third person etc
First person follows object, but looks past object
Third person both follows and looks at object
Global is set at a fixed position, but looks at object

Y - Toggle between which object to follow
Starts at Deer, press for Duck, press again for Plane, press again for Deer etc

You can change sideway direction of both duck and deer, and the direction they look is the direction they'll walk towards or back from
Plane will also move in the direction it looks, however it is an continues movement, it can't move backwards and has a speed button (W)


- Control object
W - Forward
S - Backwards

Plane cannot use S (backwards), and W is not forward it is higher speed


- Control camera/direction
Not in use when in global camera mode
ARROW UP - look up
ARROW LEFT - look left
ARROW DOWN - look down
ARROW RIGHT - look right

Deer and Duck cannot use ARROW UP(look up) and ARROW DOWN (look down)



## Other comments

Object tend to clip in the wall/terrain if height between vertecies has too great of a distance
Best seen with duck or deer in mountain.
Object can also clip if they turn beside a wall.

Objects move by themself if user doesn't have control, but they are stupid

Controls are has a little lag and can be a bit hard to use

Sunrise goes back a tiny bit, before actually rising, and light transitions arent the smoothest

For my PC, a decent work PC, everytime water animates(update), there's a slight halt in movement



## Sources
All assets are either made myself or from given resources except cloud:

https://freepng.pictures/download/cloud-3/



