#include <moving_objects.h>

#include <global.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

MovObj::MovObj(GLuint PID, char objID)
{
	id = objID;

	// Needs to be inizialized before if, with fun suprise if bug
	char * model = "../res/objects/cube.obj";
	char * texture = "../res/textures/peter.dds";


	// Specifiecs the spesifiks of each object
	if (id == 'P')		// If plane
	{
		model = "../res/objects/Plane.obj";
		texture = "../res/textures/Bark_Tile.jpg";

		xPos = 0.0f;
		yPos = 10.0f;
		zPos = 0.0f;

		threeDDir = glm::vec3(0, 0, 1);
	}
	else if (id == 'D')	// If Deer
	{
		model = "../res/objects/Deer.obj";
		texture = "../res/textures/Deer.jpg";

		xPos = 0.0f;
		yPos = 0.0f;
		zPos = 0.0f;
		threeDDir = glm::vec3(1, 0, 0);
	}
	else if (id == 'B')	// If Duck (bird)
	{
		model = "../res/objects/DuckMomHP.obj";
		texture = "../res/textures/DuckMomHR.jpg";

		xPos = 5.0f;
		yPos = 0.0f;
		zPos = 0.0f;
		threeDDir = glm::vec3(0, 0, 1);
	}

	up = glm::vec3(0, 1, 0);

	Model_3D tmpObj(model, texture, PID);
	object = tmpObj;
	//object.setRotation(90);

	model = NULL;
	delete model;
	texture = NULL;
	delete texture;
}



// Returns the coaridantes when asked for
glm::vec3 MovObj::getPos()
{
	return glm::vec3(xPos, yPos, zPos);
}

glm::vec3 MovObj::getDir()
{
	return threeDDir;
}

glm::vec3 MovObj::getUp()
{
	return up;
}






// Function to choose which movement function to use
void MovObj::move(const double dt, Level l)
{
	// This function exist to avoid thinking about which objects type moves in which way
	// So if object is P we use the three dimentional movment, else twodmove
	if (id == 'P')
	{
		threeDMovment(dt, l);
	}
	else
	{
		twoDMovment(dt, l);
	}
}


// Gets next dir
void MovObj::changeDir(int d)
{
	float nextDir;	// Next desired direction

	// makes sure it's always 0-4
	if (d == 9)	// If d is 9, get random dir
	{
		nextDir = rand() % 5;
	}
	else		// d is desired dir
	{
		nextDir = d;
	}

	if (nextDir < 0)
	{
		nextDir = 4;
	}
	else if (nextDir > 4)
	{
		nextDir = 0;
	}


	// chages direction
	if (id == 'P')
	{
		if (nextDir == 1) // move left
		{
			threeDDir = glm::rotate(threeDDir, glm::radians(turnSpeed), up);
			object.rotate(turnSpeed, glm::vec3(0,1,0));
		}
		else if (nextDir == 2) // move up
		{
			glm::vec3 right = glm::normalize(glm::cross(threeDDir, up));
			threeDDir = glm::rotate(threeDDir, glm::radians(turnSpeed), right);
			up = glm::rotate(up, glm::radians(turnSpeed), right);
			object.rotate(-turnSpeed, glm::vec3(1,0,0));
		}
		else if (nextDir == 3) // move right
		{
			threeDDir = glm::rotate(threeDDir, glm::radians(-turnSpeed), up);
			object.rotate(-turnSpeed, glm::vec3(0, 1, 0));
		}
		else if (nextDir == 4) // move down
		{
			glm::vec3 right = glm::normalize(glm::cross(threeDDir, up));
			threeDDir = glm::rotate(threeDDir, glm::radians(-turnSpeed), right);
			up = glm::rotate(up, glm::radians(-turnSpeed), right);
			object.rotate(turnSpeed, glm::vec3(1, 0, 0));
		}
	}
	else 
	{
		if (nextDir == 1 || nextDir == 3) // Move left
		{
			threeDDir = glm::rotate(threeDDir, glm::radians(turnSpeed), glm::vec3(0,1,0));
			object.rotate(turnSpeed, glm::vec3(0, 1, 0));
		}
		else if (nextDir == 2 || nextDir == 4) // Move right
		{
			threeDDir = glm::rotate(threeDDir, glm::radians(-turnSpeed), glm::vec3(0,1,0));
			object.rotate(-turnSpeed, glm::vec3(0, 1, 0));
		}
	}

	glm::normalize(threeDDir);
	glm::normalize(up);
}


// Gets next movement
void MovObj::changeMov(int d)
{
	// makes sure it's always 1-4
	if (d == 9)	// If d is 9, get random dir
	{
		nextMove = rand() % 2;
	}
	else		// d is desired dir
	{
		nextMove = d;
	}

	if (nextMove < 0)
	{
		nextMove = 2;
	}
	else if (nextMove > 2)
	{
		nextMove = 0;
	}
}



// Movement for objects and can only move "2" ways
void MovObj::twoDMovment(const double dt, Level l)
{
	// To look at next position to make sure it's safe
	float nextX = xPos;
	float nextZ = zPos;

	// Limts places to move, based on height
	float maxH = 0.0f;
	float minH = 0.0f;

	// Obsicals
	float height;
	bool tree;
	float roamingBox = LVLSIZE / 2 - 1;
	

	// Specifies how high or low each object are allowed to go
	if (id == 'D')
	{
		maxH = 2.4f;
		minH = -0.1f;
		
	}
	else if (id == 'B')
	{
		maxH = 0.4f;
		minH = -0.6f;

		roamingBox += 10;
	}

	// Calculates next position
	if (nextMove == 1)
	{
		nextX = xPos + threeDDir.x * (dt * keySpeed);
		nextZ = zPos + threeDDir.z * (dt * keySpeed);
	}
	else if (nextMove == 2)
	{
		nextX = xPos - threeDDir.x * dt * keySpeed;
		nextZ = zPos - threeDDir.z * dt * keySpeed;
	}

	float checkX = glm::round((nextX + LVLSIZE/2) * res);
	float checkZ = glm::round((nextZ + LVLSIZE/2) * res);

	float testx = glm::round(checkX + threeDDir.x);
	float testz = glm::round(checkZ + threeDDir.z);

	// Checks if new position is legal
	height = (l.lvlHeigth(checkX, checkZ)+l.lvlHeigth(testx, testz))/2;
	tree = l.treeObsticle(checkX, checkZ);

	//Makes sure duck is on water
	if (id == 'B' && height < 0.0f)
	{
		height = 0.0f;
	}

	// If position is legal, update directions and positions
	if (maxH > height && minH < height && tree == false && nextX > -roamingBox && nextZ > -roamingBox && nextZ < roamingBox && nextX < roamingBox)
	{
		nextMove = 0;

		xPos = nextX;
		yPos = height;
		zPos = nextZ;
	}
}


// Movement for objects and can move 3 ways
void MovObj::threeDMovment(const double dt, Level l)
{
		float maxH = 30.0f;
		float minH = 4.0f;

		// To look at next position to make sure it's safe
		float nextX = xPos;
		float nextY = yPos;
		float nextZ = zPos;

		// Obsicals
		float height;
		float roamingBox = LVLSIZE-5;

		float newKeySpeed = keySpeed;
		// Calculates next position
		if (nextMove == 1)
		{
			newKeySpeed *= 5;
		}

		nextX = xPos + threeDDir.x * (dt * newKeySpeed);
		nextY = yPos + threeDDir.y * (dt * newKeySpeed);
		nextZ = zPos + threeDDir.z * (dt * newKeySpeed);


		float checkX = glm::round((nextX + LVLSIZE / 2) * res);
		float checkZ = glm::round((nextZ + LVLSIZE / 2) * res);

		float testx = glm::round(checkX + threeDDir.x);
		float testz = glm::round(checkZ + threeDDir.z);

		// Checks if new position is legal
		height = (l.lvlHeigth(checkX, checkZ) + l.lvlHeigth(testx, testz)) / 2;


		// If position is legal, update directions and positions
		if (nextY > height && maxH > nextY && minH < nextY && nextX > -roamingBox && nextZ > -roamingBox && nextZ < roamingBox && nextX < roamingBox)
		{
			nextMove = 0;

			xPos = nextX;
			yPos = nextY;
			zPos = nextZ;
		}
}


// Checks if another object is on specified coordinates
bool MovObj::objObsticlae(float x, float z)
{
	// If both x and z matches return true
	if (zPos == z && xPos == x)
	{
		return true;
	}
	else
	{
		return false;
	}
}


// Draws Objects
void MovObj::draw(glm::mat4 proj, glm::mat4 view)
{	
	// draw
	object.draw(glm::vec3(xPos, yPos, zPos), proj, view);
}


// Updates light position, colour, and send degrees
void MovObj::updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt)
{
	object.updateLight(l, lc, d, dt);
}