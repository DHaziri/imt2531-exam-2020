#include <model_2D.h>
#include <graphics_functions.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

Model_2D::Model_2D()
{
    
}

Model_2D::Model_2D(char const* texPath, GLuint PID)
    : ProgramID{PID}
{
	glGenVertexArrays(1, &vertexArrayID);

	glGenBuffers(1, &vertexbuffer);

	glGenBuffers(1, &uvbuffer);

	glGenBuffers(1, &elementbuffer);

	TextureID = glGetUniformLocation(ProgramID, "myTextureSampler");

	// Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

    texture = Graphics::loadTexture(texPath);
}


// To place the 2D sprites on the screen
void Model_2D::make_square(glm::vec2 pos, float scale)
{
	// Sets each corner of the square
	indexed_vertices.clear();
	indexed_vertices.push_back(glm::vec2(pos.x - scale, pos.y - scale));
	indexed_vertices.push_back(glm::vec2(pos.x + scale, pos.y - scale));
	indexed_vertices.push_back(glm::vec2(pos.x - scale, pos.y + scale));
	indexed_vertices.push_back(glm::vec2(pos.x + scale, pos.y + scale));

	// Sets which parts of the sprite that should be drawn (which is everything)
	indexed_uvs.clear();
	indexed_uvs.push_back(glm::vec2(0, 0));
	indexed_uvs.push_back(glm::vec2(1, 0));
	indexed_uvs.push_back(glm::vec2(0, 1));
	indexed_uvs.push_back(glm::vec2(1, 1));

	// Set which verticies each triangle building the square should use
	indices.clear();
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(2);
	indices.push_back(1);


	setBufferData();
}



void Model_2D::setBufferData()
{

	glBindVertexArray(vertexArrayID);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec2), &indexed_vertices[0], GL_STATIC_DRAW);



	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);
	

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
}




void Model_2D::draw()
{
	glUseProgram(ProgramID);

    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);


	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);


    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);



    // Draw the triangles !
    glDrawElements(
        GL_TRIANGLES,      // mode
        indices.size(),    // count
        GL_UNSIGNED_SHORT,   // type
        (void*)0           // element array buffer offset
    );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}