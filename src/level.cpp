#include <level.h>

#include <global.h>

#include <cstdlib>
#include <iostream>



Level::Level(GLuint PID, GLuint GID, GLuint WID, GLuint GUIPID)
{
	// Generate the ground, water and sky
	buildGround(GID);
	buildWater(WID);
	buildSky(PID);

	randomizeTrees();
	randomizeClouds();

	// initilizes the objecyts
	Model_3D tmpTree("../res/objects/pine_tree_fan.obj", "../res/textures/pine_tree_fan.png", PID);
	tree = tmpTree;
	Model_3D tmpCloud("../res/objects/Cloud.obj", "../res/textures/Cloud.png", PID);
	cloud = tmpCloud;
}


// Draws the level
void Level::draw(GLuint WID, glm::mat4 proj, glm::mat4 view)
{
	// Build water anew each time to "animate"
	if (aniTimer > 0)				// For unsmooth fiks
	{
		aniTimer--;
	}
	else
	{
		buildWater(WID);
		aniTimer = 30;
	}

	// Draws/Generates ground, water and sky
	ground.draw(glm::vec3(0, 0, 0), proj, view);
	water.draw(glm::vec3(-LVLSIZE*res/2, 0.0, -LVLSIZE*res/2), proj, view);
	skyBox.draw(glm::vec3(0, 0, 0), proj, view);

	// Draws all the generated trees
	for (int i = 0; i < amountTrees; i++)
	{
		tree.draw(glm::vec3(treesPosition[i].x, treesPosition[i].y, treesPosition[i].z), proj, view);
	}

	// Draws all the generated clouds
	for (int i = 0; i < amountClouds; i++)
	{
		cloud.draw(glm::vec3(cloudPosition[i].x, cloudPosition[i].y, cloudPosition[i].z), proj, view);
	}
}



// Creates a randomized textured ground
void Level::buildGround(GLuint GID)
{
	// Creates the size of the terrain
	unsigned short terrain = LVLSIZE*res+2;

	// Gets lakepaths sizes
	unsigned short waterThickness = 2*res;
	unsigned short waterStart = terrain / 3 * 2;;
	unsigned short waterEnd = waterStart + waterThickness;;
	
	// Gets mountain top
	unsigned short mountTop = (terrain / 6) * 2;
	
	float downMinH;			// To generate the lowest heights going down
	float upMinH;			// To generate the lowest hight hoing up
	int genHDiff = 80/res;		// The allowd height difference between vertecies
	int amountDes = 100;	// How many desimals to use
	
	// To get the height / y coordiante of the vertecis
	float height;

	// To generate the faces
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	// Resize vector to get z axis
	yPosition.resize(terrain);

	// Generetes the Terrain
	for (int z = 0; z < terrain; z++)		// z axis
	{
		// Reset hill heights before every new line
		downMinH = 2.5f;
		upMinH = -0.1f;

		// Resize vector to get x axis
		yPosition[z].resize(terrain);

		for (int x = 0; x < terrain; x++)	// x axis
		{
			// "Beutify the edges
			if (x < 1 || z < 1 || z > terrain - 2 || x > terrain - 2)
			{
				height = -1.0;
			}
			else if ((z == 1 && x == 1) || (z == terrain - 1 && x == 1) || (z == terrain - 1 && x == terrain - 1))
			{
				height = 0.2;
			}
			else if ((z < 3 && x < 3) || (z > terrain - 3 && x < 3) || (z > terrain - 3 && x > terrain - 3))
			{
				height = 0.4;
			}


			// Generes lakepath
			//	Draws lakepath if it is between specified area along the x axis
			else if(x >= waterStart && x <= waterEnd)
			{ 
				height = -0.5;

				if (x == waterEnd) // When x axis is finished, change course of lake
				{
					// Limits the rage it can travel
					if (rand() % 2 == 0 && waterStart - 1 > terrain / 2)
					{
						waterStart--;
					}
					else if (waterStart < (terrain/8)*7)
					{
						waterStart++;
					}

					// Generates both lake width and where it ends along the x axis
					waterThickness = rand() % 5 + 2;
					waterEnd = waterStart + waterThickness;
				}
			}


			// Generetes moutain
			//	Sets top height
			else if (z == mountTop && x == mountTop)
			{
				height = 10.0f;
			}
			//	Creates surrings mountain
			else if (z > (terrain / 6) && x > (terrain / 6) && z < (terrain / 6) * 3  && x < (terrain /6) * 3 )
			{
				float furthurAway = 1.0f;
				float awayX = x - mountTop;
				float awayZ = z - mountTop;


				// Makes sure they're not minus, so we can compare
				if (awayX < 0)
				{
					awayX *= -1.0;
				}

				if (awayZ < 0)
				{
					awayZ *= -1.0;
				}

				// Finds the bigger distance to change the 
				//	vertex height based on the furtheres distance
				if (awayX > awayZ)
				{
					furthurAway = awayX;
				}
				else
				{
					furthurAway = awayZ;
				}


				// Limits the lowest height the verticies can be
				float minHeight = 5.0*res - furthurAway;
				if (minHeight < 0.25*res)
				{
					minHeight = 0.25*res;
				}


				// Limits the difference in heights the verticies can be
				int heightDiff = (rand() % 5 + (furthurAway/3 * amountDes));
				if (heightDiff > 250 * res)
				{
					heightDiff = 250*res;
				}

				height = (rand() % (heightDiff) + (minHeight * amountDes)) / amountDes;
			}


			// Generates the hills/"vally"
			//	Generates "beach" corner
			else if (x > (terrain / 3) * 2 && z < terrain / 3)
			{
				float plus = 3.0 * ((z*1.5) + (-1.0*(x/3.0)));
				height = (rand() % (genHDiff)+(plus-50)) / amountDes;
			}
			//	Generates the hill on the beachside of the lake
			else if (x > (terrain / 3)*2)
			{
				height = (rand() % (genHDiff)+(upMinH* amountDes)) / amountDes;
			}
			// Generates the curved hill squared in by the mountain and the lake
			else if (z > terrain / 2 && x < terrain / 2)
			{
				float plus = -1*glm::pow(x-terrain/4, 2)/2+150;
				height = (rand() % (genHDiff)+(plus)) / amountDes;
			}
			// Generates everything else, creating a hill going down to the lake
			else
			{
				height = (rand() % (genHDiff)+(downMinH  * amountDes)) / amountDes;
			}


			// Adds the verteces and co to the back of the "list"/vector
			vertices.push_back(glm::vec3((1.0 / res * x) - LVLSIZE/2, height, (1.0 / res * z) - LVLSIZE / 2));
			uvs.push_back(glm::vec2(x % 2, z % 2));
		//	normals.push_back(glm::vec3(0, 1, 0));


			// Saves height/ y position
			yPosition[z][x] = height;


			// Takes hill heights a step up/down
			if (x % 6/res == 0 && x > (terrain / 3) * 2)
			{
				upMinH += 0.6/res;
			}

			if (x % 6/res == 0)
			{
				downMinH -= 0.4/res;
			}
		}
	}


	// Adds the normals
	// by getting the sides of the triangles that is connected to the vertcies
	// getting the normal of each of these triangles, and adding them together, to get the actual normal
	for (int z = 0; z < terrain; z++)
	{
		for (int x = 0; x < terrain; x++)
		{			
			// Collects each vertex, triangle normal
			std::vector<glm::vec3> triNorm;

			//Ifs to make sure it's not on the edge
			if (x > 0)
			{
				if (z > 0)
				{
					glm::vec3 side1 = vertices[x - 1 + z * terrain] - vertices[x + z * terrain];
					glm::vec3 side2 = vertices[x + (z - 1)* terrain] - vertices[x + z * terrain];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));

				}
				if (z < terrain - 1)
				{
					glm::vec3 side1 = vertices[x - 1 + z * terrain] - vertices[x + z * terrain];
					glm::vec3 side2 = vertices[x + (z + 1)* terrain] - vertices[x + z * terrain];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));
				}

			}
			if (x < terrain - 1)
			{
				if (z > 0)
				{
					glm::vec3 side1 = vertices[x + 1 + z * terrain] - vertices[x + z * terrain];
					glm::vec3 side2 = vertices[x + (z - 1)* terrain] - vertices[x + z * terrain];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));

				}
				if (z < terrain - 1)
				{
					glm::vec3 side1 = vertices[x + 1 + z * terrain] - vertices[x + z * terrain];
					glm::vec3 side2 = vertices[x + (z + 1)* terrain] - vertices[x + z * terrain];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));
				}
			}

			glm::vec3 sum = glm::vec3(0, 0, 0);
			for (int i = 0; i < triNorm.size(); i++)
			{
				sum += triNorm[i];
			}

			normals.push_back(sum);
		}
	}


	// Adds indices in a square, to botht make sure everything fits together
	//	and to make sure all faces are drawn in the same direction to avoid losing
	//	faces when culling
	for (unsigned short i = 0; i < terrain - 1; i++)
	{
		for (unsigned short j = 0; j < terrain - 1; j++)
		{
			indices.push_back(i*terrain + j + (terrain));
			indices.push_back(i*terrain + j + 1);
			indices.push_back(i*terrain + j);

			indices.push_back(i*terrain + j + 1);
			indices.push_back(i*terrain + j + (terrain));
			indices.push_back(i*terrain + j + (terrain)+1);
		}
	}


	// Create model
	Model_3D tmpGround("../res/textures/peter.dds", GID, vertices, uvs, normals, indices, 'G');
	ground = tmpGround;
}






// Creates a randomized water
void Level::buildWater(GLuint WID)
{
	// Creates the size of the water
	unsigned short seaSize = (LVLSIZE * 2 + 4) * res;


	// To generate the faces
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	   

	//Genereates the water
	for (int z = 0; z < seaSize; z++)
	{
		for (int x = 0; x < seaSize; x++)
		{
			// Generates random height and adds vertecies and uvs to the list/vector
			float height = (((rand() % 10) - 5.0) / 100.0);
			vertices.push_back(glm::vec3((1.0 / res * x), height, (1.0 / res * z) ));
			uvs.push_back(glm::vec2(x % 2, z % 2));
		}
	}


	//Adds the normals
	// by getting the sides of the triangles that is connected to the vertcies
	// getting the normal of each of these triangles, and adding them together, to get the actual normal
	for (int z = 0; z < seaSize; z++)
	{
		for (int x = 0; x < seaSize; x++)
		{
			// Collects each vertex, triangle normal
			std::vector<glm::vec3> triNorm;
			
			//Ifs to make sure it's not on the edge
			if ( x > 0)
			{
				if (z > 0)
				{
					glm::vec3 side1 = vertices[x - 1 + z * seaSize] - vertices[x + z * seaSize];
					glm::vec3 side2 = vertices[x + (z - 1)* seaSize] - vertices[x + z * seaSize];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));
				}
				if (z < seaSize-1)
				{
					glm::vec3 side1 = vertices[x - 1 + z * seaSize] - vertices[x + z * seaSize];
					glm::vec3 side2 = vertices[x + (z + 1)* seaSize] - vertices[x + z * seaSize];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));
				}
			}
			if (x < seaSize-1)
			{
				if (z > 0)
				{
					glm::vec3 side1 = vertices[x + 1 + z * seaSize] - vertices[x + z * seaSize];
					glm::vec3 side2 = vertices[x + (z - 1)* seaSize] - vertices[x + z * seaSize];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));

				}
				if (z < seaSize - 1)
				{
					glm::vec3 side1 = vertices[x + 1 + z * seaSize] - vertices[x + z * seaSize];
					glm::vec3 side2 = vertices[x + (z + 1)* seaSize] - vertices[x + z * seaSize];

					triNorm.push_back(glm::normalize(glm::cross(side1, side2)));
				}

			}

			glm::vec3 sum = glm::vec3(0, 0, 0);
			for (int i = 0; i < triNorm.size(); i++)
			{
				sum += triNorm[i];
			}

			normals.push_back(sum);
		}
	}



	// Adds indices in a square, to botht make sure everything fits together
	//	and to make sure all faces are drawn in the same direction to avoid losing
	//	faces when culling
	for (unsigned short i = 0; i < seaSize - 1; i++)
	{
		for (unsigned short j = 0; j < seaSize - 1; j++)
		{
			indices.push_back(i*seaSize + j + (seaSize));
			indices.push_back(i*seaSize + j + 1);
			indices.push_back(i*seaSize + j);

			indices.push_back(i*seaSize + j + 1);
			indices.push_back(i*seaSize + j + (seaSize));
			indices.push_back(i*seaSize + j + (seaSize)+1);
		}
	}

	// Create model
	if (firstInizalizing) 
	{
		Model_3D tmpWater("../res/textures/blue.jpg", WID, vertices, uvs, normals, indices, 'W');
		water = tmpWater;
		firstInizalizing = false;
	}
	else
	{
		water.updateModel(vertices, uvs, normals, indices);
	}
}







// Creates the skybox
void Level::buildSky(GLuint PID)
{
	// To generate the faces
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	int cube = LVLSIZE;			// If cube is same size as LVLSIZE, sky box is twice as big as level


	// Fill inn placements around level, 
	//	find where to look for what on texture, 
	//	face them the right way
	//	add to list/vector
	// Sky
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(cube, cube, -cube));

	uvs.push_back(glm::vec2(0.25, 1));
	uvs.push_back(glm::vec2(0.5, 1));
	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.66));

	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));

	// West
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, -cube));

	uvs.push_back(glm::vec2(0, 0.66));
	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0, 0.33));
	uvs.push_back(glm::vec2(0.25, 0.33));

	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));

	// North
	vertices.push_back(glm::vec3(cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));

	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.66));
	uvs.push_back(glm::vec2(0.25, 0.33));
	uvs.push_back(glm::vec2(0.5, 0.33));

	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));

	// East
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));

	uvs.push_back(glm::vec2(0.5, 0.66));
	uvs.push_back(glm::vec2(0.75, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.33));
	uvs.push_back(glm::vec2(0.75, 0.33));

	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));

	// South
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));

	uvs.push_back(glm::vec2(0.75, 0.66));
	uvs.push_back(glm::vec2(1, 0.66));
	uvs.push_back(glm::vec2(0.75, 0.33));
	uvs.push_back(glm::vec2(1, 0.33));

	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));

	// Ground
	vertices.push_back(glm::vec3(cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));

	uvs.push_back(glm::vec2(0.25, 0.33));
	uvs.push_back(glm::vec2(0.5, 0.33));
	uvs.push_back(glm::vec2(0.25, 0));
	uvs.push_back(glm::vec2(0.5, 0));

	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));



	// Adds indices in a square, to botht make sure everything fits together
	//	and to make sure all faces are drawn in the same direction to avoid losing
	//	faces when culling
	for (int i = 0; i < 6; i++)
	{
		indices.push_back(i * 4 + 2);
		indices.push_back(i * 4 + 1);
		indices.push_back(i * 4);

		indices.push_back(i * 4 + 1);
		indices.push_back(i * 4 + 2);
		indices.push_back(i * 4 + 3);
	}

	// Create model
	Model_3D tmpSky("../res/textures/Sky.png", PID, vertices, uvs, normals, indices, 'S');
	skyBox = tmpSky;
}


// Randomizes places of the trees
void Level::randomizeTrees()
{
	// Get desired amount of trees
	amountTrees = rand() % 10 + 10;

	int k = -1; // To keep track of how long temp list is

	std::vector<glm::vec3> tmp;

	// Goies though amount of trees
	for (int i = 0; i < amountTrees; i++)
	{
		int z = 0;
		int x = 0;

		
		// As long as either z or x is is 0
		while (z == 0 || x == 0)
		{
			// Get random placements
			z = rand() % (LVLSIZE)*res+2;
			x = rand() % (LVLSIZE)*res+2;

			tmp.push_back(glm::vec3((1.0 / res * x) - LVLSIZE / 2, yPosition[z][x], (1.0 / res * z) - LVLSIZE / 2));
			k++;

			// Loop to go through previously filled list to avoid duplicates
			for (int j = i-1; j >= 0; j--)
			{
				// If any of the unwated conditions apply
				if (treesPosition[j] == tmp[k] || yPosition[z][x] < 0.8f || yPosition[z][x] > 3.0f || x > LVLSIZE*res+2 || z > LVLSIZE*res+2)
				{
					// Make z and x 0
					z = 0;
					x = 0;
					break;
				}
			}
		}
		// Add tree to list if while loop ended
		treesPosition.push_back(glm::vec3((1.0 / res * x) - LVLSIZE / 2, yPosition[z][x], (1.0 / res * z) - LVLSIZE / 2));
	}
}



void Level::randomizeClouds()
{
	// Get desired amount of clouds
	amountClouds = rand() % 10 + 5;
	float maxHeight = LVLSIZE;

	for (int i = 0; i < amountClouds; i++) 
	{
		float height = (rand() % (10*10) + (7.5 * 10)) / 10;
		float x = (rand() % (LVLSIZE * 2) * 10 - LVLSIZE * 10) / 10;
		float z = (rand() % (LVLSIZE * 2) * 10 - LVLSIZE * 10) / 10;

	//	if (height)
		cloudPosition.push_back(glm::vec3(x, height, z));
	}
}


// Moves cloud
void Level::moveCloud(const double dt)
{
	for (int i = 0; i < amountClouds; i++)
	{
		// if cloud is beond skybox
		if (cloudPosition[i].z > LVLSIZE + 5)
		{
			cloudPosition[i].z = -(LVLSIZE + 5);
		}

		cloudPosition[i].z +=  dt * cloudSpeed;
	}
}





// Checks heigth at given coordinates
float Level::lvlHeigth(float x, float z)
{
	if (z >= yPosition.size() || z < 0 || x >= yPosition[0].size() || x < 0 )
	{
		return 0;
	}
	return yPosition[z][x];
}





// Checks if there's a three at given coordinates
bool Level::treeObsticle(float x, float z)
{
	for (int i = 0; i < amountTrees; i++)
	{
		// If both x or z matches return true
		if (treesPosition[i][2] == z && treesPosition[i][0] == x)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}


// Updates light position, colour, and send degrees
void Level::updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt)
{
	ground.updateLight(l, lc, d, dt);
	water.updateLight(l, lc, d, dt);
	skyBox.updateLight(l, lc, d, dt);
	tree.updateLight(l, lc, d, dt);
}