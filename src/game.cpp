#include <game.h>

#include <graphics_functions.h>
#include <global.h>
#include <model_2D.h>

#include <sstream>
#include <iostream>
#include <time.h>
#include <cmath>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>


Game::Game()
{
    // Initialise GLFW
    if (!glfwInit())
    {
        fprintf(stderr, "Failed to initialize GLFW\n");
        exit(-1);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(1024, 768, "pacman", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n");
        glfwTerminate();
        exit(-1);
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        glfwTerminate();
        exit(-1);
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Dark blue background
    glClearColor(0.1f, 0.3f, 0.5f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);/**/

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

    programID = Graphics::loadShaders("../res/shaders/shader3D.vert", "../res/shaders/shaderLight.frag");
	groundID = Graphics::loadShaders("../res/shaders/shader3D.vert", "../res/shaders/shaderGround.frag");
	waterID = Graphics::loadShaders("../res/shaders/shader3D.vert", "../res/shaders/shaderLight.frag");
	GUIProgramID = Graphics::loadShaders("../res/shaders/shader2D.vert", "../res/shaders/shader.frag");


	srand(time(NULL));
}



// The main game, acts as a new int main()
void Game::start() 
{
	sunPosition = glm::vec3(0, 20, 0);
	sunColour = glm::vec3(1, 1, 0);

	// Initialize level and objects
	Level lvltmp(programID, groundID, waterID, GUIProgramID);
	level = lvltmp;

	MovObj tmpP(programID, 'P');
	MovObj tmpD(programID, 'D');
	MovObj tmpB(programID, 'B');
	plane = tmpP;
	deer = tmpD;
	duck = tmpB;


    // Projection matrix : 45� Field of View, 16:9 ratio, display range : 0.1 unit <-> 100 units
    Projection = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.1f, 100.0f);

    // Camera matrix
    View = glm::lookAt(
        glm::vec3(25, 30, -15), // Camera is at (0,3,6), in World Space
        glm::vec3(0, 0, 0), // and looks at the origin
        glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
    );

	auto time_last_frame = glfwGetTime();
	

    do 
	{
        // Clear the screen.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Compute delta time 
		const auto dt = glfwGetTime() - time_last_frame;
		time_last_frame = glfwGetTime();


		// Updates Mechanics, then draws
		mechanics(dt);	
		drawGame(dt);
				

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
        glfwWindowShouldClose(window) == 0);


    // Close OpenGL window and terminate GLFW
    glfwTerminate();
}








// Handles the controls of the game
void Game::controls(GLFWwindow * window, int key, int scancode, int action, int mods)
{	
	if (controlMode == PLANE)
	{
		if (key == GLFW_KEY_W && action == GLFW_PRESS)
		{
			plane.changeMov(1);
		}
		else if (key == GLFW_KEY_LEFT && action == GLFW_REPEAT)
		{
			plane.changeDir(1);
		}
		else if (key == GLFW_KEY_UP && action == GLFW_REPEAT)
		{
			plane.changeDir(2);
		}
		else if (key == GLFW_KEY_RIGHT && action == GLFW_REPEAT)
		{
			plane.changeDir(3);
		}
		else if (key == GLFW_KEY_DOWN && action == GLFW_REPEAT)
		{
			plane.changeDir(4);
		}
		else if (key == GLFW_KEY_T && action == GLFW_PRESS)
		{
			changeCam = true;		// Ask to change camera
		}
		else if (key == GLFW_KEY_Y && action == GLFW_PRESS)
		{
			changeControl = true; 	// Ask to change who to control
		}

	}

	else if (controlMode == DEER)
	{
		if (key == GLFW_KEY_W && action == GLFW_REPEAT)
		{
			deer.changeMov(1);		//forward
		}
		else if (key == GLFW_KEY_S && action == GLFW_REPEAT)
		{
			deer.changeMov(2);	// backward
		}
		else if (key == GLFW_KEY_LEFT && action == GLFW_REPEAT)
		{
			deer.changeDir(1);		// left
		}
		else if (key == GLFW_KEY_RIGHT && action == GLFW_REPEAT)
		{
			deer.changeDir(2);		// right
		}
		else
		{
			deer.changeMov(0);		// no move or dir
		}		
		
		if (key == GLFW_KEY_T && action == GLFW_PRESS)
		{
			changeCam = true;		// Ask to change camera
		}
		else if (key == GLFW_KEY_Y && action == GLFW_PRESS)
		{
			changeControl = true;	// Ask to change who to control
		}
	}

	else if (controlMode == DUCK)
	{
		if (key == GLFW_KEY_W && action == GLFW_REPEAT)
		{
			duck.changeMov(1);		//forward
		}
		else if (key == GLFW_KEY_S && action == GLFW_REPEAT)
		{
			duck.changeMov(2);	// backward
		}
		else if (key == GLFW_KEY_LEFT && action == GLFW_REPEAT)
		{
			duck.changeDir(1);		// left
		}
		else if (key == GLFW_KEY_RIGHT && action == GLFW_REPEAT)
		{
			duck.changeDir(2);		// right
		}
		else
		{
			duck.changeMov(0);		// no move or dir
		}

		if (key == GLFW_KEY_T && action == GLFW_PRESS)
		{
			changeCam = true;		// Ask to change camera
		}
		else if (key == GLFW_KEY_Y && action == GLFW_PRESS)
		{
			changeControl = true;	// Ask to change who to control
		}
	}


	// Is asked to change what to control
	// Can only be choosen in a circle
	if (changeControl == true)
	{
		if (controlMode == PLANE)
		{
			controlMode = DEER;
		}
		else if (controlMode == DEER)
		{
			controlMode = DUCK;
		}
		else if (controlMode == DUCK)
		{
			controlMode = PLANE;
		}

		changeControl = false;
	}


	// Camera mode is asked to change
	// Can only be choosen in a circle
	if (changeCam == true)
	{
		if (cameraMode == FIRST)
		{
			cameraMode = THIRD;
		}
		else if (cameraMode == THIRD)
		{
			cameraMode = GLOBAL;
		}
		else if (cameraMode == GLOBAL)
		{
			cameraMode = FIRST;
		}

		changeCam = false;
	}
}



// Handels the mechanics of the game
void Game::mechanics(const double dt)
{
	// Updates sun position, colour and degree,
	//	and then send everything else the info
	updateSun(dt);

	level.updateLight(sunPosition, sunColour, degrees, dt);
	plane.updateLight(sunPosition, sunColour, degrees, dt);
	deer.updateLight(sunPosition, sunColour, degrees, dt);
	duck.updateLight(sunPosition, sunColour, degrees, dt);


	// If choosen make sure both camera and controls 
	//	 focus on right object
	if (controlMode == PLANE)
	{
		updateCamera(dt, plane);
		deer.changeDir();
		duck.changeDir();
		deer.changeMov();
		duck.changeMov();
	}
	else if (controlMode == DEER)
	{
		updateCamera(dt, deer);
		plane.changeDir();
		duck.changeDir();
		duck.changeMov();
	}
	else if (controlMode == DUCK)
	{
		updateCamera(dt, duck);
		plane.changeDir();
		deer.changeDir();
		deer.changeMov();
	}
	
	// move objects
	level.moveCloud(dt);
	plane.move(dt, level);
	deer.move(dt,level);
	duck.move(dt, level);
}



// Draws out the grafics
void Game::drawGame(const double dt)
{
	// In level.draw only buildWater needs programID so need to send spesific water ID
	level.draw(waterID, Projection, View);

	if (!(controlMode == PLANE && cameraMode == FIRST))
	{
		plane.draw(Projection, View);
	}
	if (!(controlMode == DEER && cameraMode == FIRST))
	{
		deer.draw(Projection, View);
	}
	if (!(controlMode == DUCK && cameraMode == FIRST))
	{
		duck.draw(Projection, View);
	}
}


// Updates camera in relation to Pacman
void Game::updateCamera(float dt, MovObj o)
{
	// Gets object cooardinates



	glm::vec3 camPos;
	glm::vec3 focusPos;
	glm::vec3 up;

	// Sets the needed camera cooridnates based on camMode
	if (cameraMode == FIRST)
	{
		camPos = o.getPos() + (o.getUp()*0.5f);
		focusPos = o.getPos() + o.getDir() + (o.getUp()*0.5f);
		up = o.getUp();
	}
	else if (cameraMode == THIRD)
	{
		camPos = o.getPos() - (o.getDir()*5.0f) + (o.getUp()*2.5f);
		focusPos = o.getPos();
		up = o.getUp();
	}
	else if (cameraMode == GLOBAL)
	{
		camPos = glm::vec3(25, 20, -5);
		focusPos = o.getPos();
		up = glm::vec3(0, 1, 0);
	}


	// Camera matrix
	View = glm::lookAt (
		camPos,	 	// Camera is at camPos, in World Space
		focusPos, // Where camera is looking at
		up							// Head is up (set to 0,-1,0 to look upside-down)
	);
}



// Updates sun position and colour
void Game::updateSun(const double dt)
{
	// Add/Remove, to get radian we can remove 180 if degrees is over 180
	// without having more then one if to check
	float addRm = 0.0f;

	// To keep it under 360
	if (degrees > 360.0)	
	{
		degrees -= 359.0;
	}
	// Make addRm 1 to make 180 valid
	else if (degrees > 180.0)	
	{
		addRm = 1.0f;		
	}

										// Get the desired degree, remove 180 if addRm is 1
	float tmpD = (degrees +  dt * sunSpeed) - 180 * addRm;
	float radian = glm::radians(tmpD);	// Get radian
	float x = LVLSIZE * sin(radian);	// Get x and y coordinates
	float y = LVLSIZE * cos(radian);

	degrees = tmpD + 180 * addRm;		// Save the new degree, and add 180 addRm is 1


	// Check if x and/or y needs to be minus
	if (degrees > 0.0 && degrees > 180.0)
	{
		x *= -1.0f;
	}

	if (degrees > 90.0 && degrees > 270.0)
	{
		y *= -1.0f;
	}


	// Transitions the colour of the sun
	// Between 0 and 180 it's only red,
	// at 225 and 315 it is yellow, 
	// and between 269 and 271 is it white
	
	if (degrees >= 355.0f)
	{
		green = 1.0f;
		blue = 1.0f;
	}
	else if (degrees >= 310.0f)
	{
		green = 1.0f;
		blue += dt * colourSpeed;
	}
	else if (degrees >= 272.0f)
	{
		green += dt * colourSpeed;
		blue = 0.0f;
	}
	else if (degrees >= 88.0f)
	{
		green = 0.0f;
		blue = 0.0f;
	}
	else if (degrees >= 50.0f)
	{
		green -= dt * colourSpeed;
		blue = 0.0f;
	}
	else if (degrees >= 5.0f)
	{
		green = 1.0f;
		blue -= dt * colourSpeed;
	}

	// Overwrite temps to actual Sun
	sunPosition = glm::vec3(x, y, 0);
	sunColour = glm::vec3(1, green, blue);
}

