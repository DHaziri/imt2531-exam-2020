#include <model_3D.h>
#include <graphics_functions.h>
#include <global.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>


Model_3D::Model_3D()
{}

// Contstuctor for imported models
Model_3D::Model_3D(char const* objPath, char const* texPath, GLuint PID)
    : ProgramID{PID}
{
	rot = glm::rotate(0.0f, glm::vec3(0, 1, 0));

	// Sets up buffers, and saves the ID's in the parameters
	glGenVertexArrays(1, &vertexArrayID);
	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &uvbuffer);
	glGenBuffers(1, &elementbuffer);
	glGenBuffers(1, &normalbuffer);

	// Gets the ID's of the uniforms
	TextureID = glGetUniformLocation(ProgramID, "myTextureSampler");
	MatrixID = glGetUniformLocation(ProgramID, "MVP");
	ViewMatrixID = glGetUniformLocation(ProgramID, "V");
	ModelMatrixID = glGetUniformLocation(ProgramID, "M");
	LightID = glGetUniformLocation(ProgramID, "LightPosition_worldspace");
	LightColorID = glGetUniformLocation(ProgramID, "LightColor");
	LightOnID = glGetUniformLocation(ProgramID, "LightOn");
	AmbientPowerID = glGetUniformLocation(ProgramID, "ambientPower");

	// Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

	// Loads object and texture
    bool res = Graphics::loadAssImp(objPath, indices, indexed_vertices, indexed_uvs, indexed_normals);
    texture = Graphics::loadTexture(texPath);

	setBufferData();
}

// Constructor for self generated "models"
Model_3D::Model_3D(char const* texPath, GLuint PID, std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i, char id)
	: ProgramID{ PID }
	, indexed_vertices{ verts }
	, indexed_uvs{ uvs }
	, indexed_normals{ normals }
	, indices{ i }
	, shaderID { id }
{
	scale = 1;

	rot = glm::rotate(0.0f, glm::vec3(0, 1, 0));

	// Sets up buffers, and saves the ID's in the parameters
	glGenVertexArrays(1, &vertexArrayID);
	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &uvbuffer);
	glGenBuffers(1, &elementbuffer);
	glGenBuffers(1, &normalbuffer);

	// Gets the ID's of the uniforms
	TextureID = glGetUniformLocation(ProgramID, "myTextureSampler");
	MatrixID = glGetUniformLocation(ProgramID, "MVP");
	ViewMatrixID = glGetUniformLocation(ProgramID, "V");
	ModelMatrixID = glGetUniformLocation(ProgramID, "M");
	LightID = glGetUniformLocation(ProgramID, "LightPosition_worldspace");
	LightColorID = glGetUniformLocation(ProgramID, "LightColor");
	LightOnID = glGetUniformLocation(ProgramID, "LightOn");
	AmbientPowerID = glGetUniformLocation(ProgramID, "ambientPower");

	// Set our "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

	// Loads texture
	texture = Graphics::loadTexture(texPath);

	setBufferData();
}



void Model_3D::setBufferData()
{
	glBindVertexArray(vertexArrayID);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
}



void Model_3D::draw(glm::vec3 pos, glm::mat4 proj, glm::mat4 view)
{
	glUseProgram(ProgramID);

    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glm::mat4 translate = glm::translate(glm::mat4(), pos);

    glm::mat4 scaleMat = glm::scale((glm::mat4(1.0f), glm::vec3(scale)));

    /*glm::vec3 myRotationAxis(0 , 1 , 0 );

   // rotation += 1;
    glm::mat4 r = glm::rotate(glm::radians(rotation), myRotationAxis);*/
    // Model matrix
    glm::mat4 Model = translate * rot * scaleMat;
	glm::mat4 invTModel = glm::inverse(glm::transpose(Model));
    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 MVP = proj * view * Model; // Remember, matrix multiplication is the other way around

    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &Model[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &view[0][0]);


	// If shader is spesific for ground(G) or water(W)
	if (shaderID == 'G')
	{

	}
	else if (shaderID == 'W')
	{

	}


	// Light/sun off
	if (degrees > 93 && degrees < 267)
	{
		lightOn = 0;
		ambientLight = 1.0f;
	}
	else // light on
	{
		lightOn = 1;

		if (degrees >= 315 && degrees >= 45)
		{
			ambientLight = 5.0f;
		}
		else if (degrees >= 267)
		{
			ambientLight += deltaTime * ambientSpeed;
		}
		else if (degrees <= 93)
		{
			ambientLight -= deltaTime * ambientSpeed;
		}
	}

	//	std::vector<glm::vec3> lightPos = l.getLights();
	glUniform3fv(LightID, 1, glm::value_ptr(sunPosition));

	//std::vector<glm::vec3> lightColor = { glm::vec3(0,0,1),glm::vec3(0,0,1),glm::vec3(0,0,1),glm::vec3(0,0,1),glm::vec3(0,0,1) };
	glUniform3fv(LightColorID, 1, glm::value_ptr(sunColour));

	glUniform1iv(LightOnID, 1, &lightOn);

	glUniform1f(AmbientPowerID, ambientLight);






	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);



    // Draw the triangles !
    glDrawElements(
        GL_TRIANGLES,      // mode
        indices.size(),    // count
        GL_UNSIGNED_SHORT,   // type
        (void*)0           // element array buffer offset
    );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void Model_3D::rescale(float s)
{
	scale = s;
}

void Model_3D::setRotation(float r)
{
	rotation = r;
	rot = glm::rotate(glm::radians(r), glm::vec3(0, 1, 0));
}

void Model_3D::rotate(float r, glm::vec3 rotationAxis)
{
	rot = glm::rotate(rot, glm::radians(r), rotationAxis);
}

void Model_3D::updateModel(std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i)
{
	indexed_vertices = verts;
	indexed_uvs = uvs;
	indexed_normals = normals;
	indices = i;

	setBufferData();
}



void Model_3D::updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt)
{
	sunPosition = l;
	sunColour = lc;
	degrees = d;
	deltaTime = dt;
}