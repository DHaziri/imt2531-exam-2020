#include <global.h>



 // Modes in the game
ControlMode controlMode = DEER;
CameraMode cameraMode = THIRD;


const float PI = 3.14159265359;		// Pi

// Ints
const int LVLSIZE = 30;		// The level is a cube, s� x,y,z all are the same size
const int LVLEGDGE = LVLSIZE / 2;
const int ANIFRAMES = 6;	// How many frames of animation this game is based on



// Speeds
//	for movements
const float keySpeed = 5.0f;
const float turnSpeed = 1;
const float cloudSpeed = 1.0f;
//	 for  light
const float sunSpeed = 1.0f;
const float colourSpeed = 0.01f;
const float ambientSpeed = 0.04f;


