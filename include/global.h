#pragma once

#include <enum.h>


// Modes in the game
extern ControlMode controlMode;
extern CameraMode cameraMode;

extern const float PI;

extern const int LVLSIZE;		// The level is a cube, s� x,y,z all are the same size
extern const int LVLEGDGE;
extern const int ANIFRAMES;		// How many frames of animation this game is based on


// Speeds
//	for movements
extern const float keySpeed;
extern const float turnSpeed;
extern const float cloudSpeed;
//	 for light
extern const float sunSpeed;
extern const float colourSpeed;
extern const float ambientSpeed;

