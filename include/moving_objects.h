#pragma once

#include <vector>
#include <model_3D.h>
#include <level.h>
#include <GL/glew.h>
#include <glm/glm.hpp>


class MovObj
{
private:
	// Objects
	Model_3D object;


	//std::vector<glm::vec3> treesPosition;
	//glm::vec2 twoDDir;
	glm::vec3 threeDDir;
	glm::vec3 up;
	

	// Cooardinates
	float xPos;
	float yPos;
	float zPos;

	int res = 2;
	char id;			// Object id to specify certian specialties

	int nextMove = 0;	// Next desired movement


public:
	MovObj() {}										// Needed to create "global" object in game
	MovObj(GLuint PID, char objID);					// Inizializes the object


	// Returns the coaridantes when asked for
	glm::vec3 getPos();
	glm::vec3 getDir();
	glm::vec3 getUp();



	void move(const double dt, Level l);			// Function to choose which movement function to use
	void changeDir(int d = 9);						// Gets next direction
	void changeMov(int d = 9);						// Gets next movement
	void twoDMovment(const double dt, Level l);		// Movement for objects and can only move "2" ways
	void threeDMovment(const double dt, Level l);	// Movement for objects and can move 3 ways


	bool objObsticlae(float x, float z);			// Checks if another object is on specified coordinates

													// Draws Objects
	void draw(glm::mat4 proj, glm::mat4 view);

													// Updates light position, colour, and send degrees
	void updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt);

};