#pragma once

#include <vector>
//#include <iostream>
#include <model_3D.h>
#include <model_2D.h>
#include <GL/glew.h>



class Level 
{
private:

	// Positions of the height and trees
	std::vector<std::vector<float>> yPosition;
	std::vector<glm::vec3> treesPosition;
	std::vector<glm::vec3> cloudPosition;

	// 3D Models
	Model_3D skyBox;
	Model_3D ground;
	Model_3D water;

	Model_3D tree;
	Model_3D cloud;

	float aniTimer = 100;
	int res = 2;
	int amountTrees;	// how many trees
	int amountClouds;

	bool firstInizalizing = true;		// For avoiding memoryleak

public:
	Level() {}							// Needed to create global level
	Level(GLuint PID, GLuint GID, GLuint WID, GLuint GUIPID);	// Inizializes the level

										// Draws the level
	void draw(GLuint WID, glm::mat4 proj, glm::mat4 view);	// Draws every RoadBlock, Pellet etc


	void buildGround(GLuint GID);		// Creates a randomized textured ground
	void buildWater(GLuint WID);		// Creates a randomized water
	void buildSky(GLuint PID);			// Creates the skybox

	void randomizeTrees();				// Randomizes places of the trees
	void randomizeClouds();				// Randomizes places of the clouds

	void moveCloud(const double dt);	// Moves cloud

	float lvlHeigth(float x, float z);	// Checks heigth at given coordinates
	bool treeObsticle(float x, float z);// Checks if there's a three at given coordinates

										// Updates light position, colour, and send degrees
	void updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt);
};