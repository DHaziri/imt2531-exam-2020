#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <model_3D.h>
//#include <level.h>
#include <moving_objects.h>

//
class Game
{
private:

	GLuint programID;
	GLuint groundID;
	GLuint waterID;
	GLuint GUIProgramID;

	glm::mat4 Projection;	// Projection matrix
	glm::mat4 View;			// Camera matrix

	glm::vec3 sunPosition;
	glm::vec3 sunColour;

	// Classes
	Level level;
	MovObj plane;
	MovObj deer;
	MovObj duck;

	double dt;

	// Variables
	int optionSelected = 0;	// Keeps track of which option user is on
	float lvlW = 0.0f;		// Level Width
	float lvlH = 0.0f;		// Level Height

	bool changeControl = false;
	bool changeCam = false;

	float degrees = 0.0f;
	float green = 1.0f;
	float blue = 1.0f;

public:

	GLFWwindow* window; // Game Window

	// Functions
	Game();

	void start();							// The main game, acts as a new int main()
											// Handles the controls of the game
	void controls(GLFWwindow * window, int key, int scancode, int action, int mods);
	void mechanics(const double dt);		// Handels the mechanics of the game
	void drawGame(const double dt);			// Draws the grafics
	void updateCamera(float dt, MovObj o);	// Updates camera in relation to Pacman


	void updateSun(const double dt);		// Updates sun position and colour
};