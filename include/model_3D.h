#pragma once

#include <GL/glew.h>
#include <vector>

// Include GLM
#include <glm/glm.hpp>

class Model_3D
{
private:
	//
    std::vector<unsigned short> indices;
    std::vector<glm::vec3> indexed_vertices;
    std::vector<glm::vec2> indexed_uvs;
    std::vector<glm::vec3> indexed_normals;

	glm::vec3 sunPosition;
	glm::vec3 sunColour;
	float degrees;
	double deltaTime;

	int lightOn = 1;
	double ambientLight = 5.0f;


	// Buffers
    GLuint vertexArrayID;
    GLuint vertexbuffer;
    GLuint uvbuffer;
    GLuint normalbuffer;
    GLuint elementbuffer;

	GLuint ProgramID;
	GLuint TextureID;

	// Uniforms
    GLuint MatrixID;		// 
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;
	GLuint LightID;			//
	GLuint LightColorID;
	GLuint LightOnID;
	GLuint AmbientPowerID;
	

    GLuint texture;
    float rotation = 0;
	glm::mat4 rot;

    float scale = 0.5f;

	char shaderID = 'O'; // G for ground shader, W for water shader, O for other shader

public:
    Model_3D();
    Model_3D(char const* objPath, char const* texPath, GLuint PID);
	Model_3D(char const* texPath, GLuint PID, std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i, char id);

	void setBufferData();

    void draw(glm::vec3 pos, glm::mat4 proj, glm::mat4 view);
	void rescale(float s);
	void setRotation(float r);
	void rotate(float r, glm::vec3 rotationAxis);
	void updateModel(std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i);

	void updateLight(glm::vec3 l, glm::vec3 lc, float d, const double dt);
};